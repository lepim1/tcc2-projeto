
# coding=utf-8
import scrapy
import csv # ver tambem em https://github.com/chorbl/nfl-reg-season-wins-scrapy-spider/tree/master/tutorial
from datetime import datetime
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
from utils import Utils

# Realiza Crawling de dados dos senadores e salva em arquivo CSV
class SenadoSpider(scrapy.Spider):
    name = 'senadospider'
    start_urls = ['http://www25.senado.leg.br/web/transparencia/sen']
    countIds=0
    rows = []

    # Metodo para forcar que o metodo spider_closed seja chamado no final da execucao
    def __init__(self):
        dispatcher.connect(self.spider_closed, signals.spider_closed)

    def spider_closed(self, spider):
        with open('senado_recurso.csv', 'wb') as csvfile:            
            spamwriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            header = ['nome', 'partido', 'recursos_utilizados', 'data_da_extracao', 'ano', 'url']
            spamwriter.writerow(header)
            for linha in self.rows:
                spamwriter.writerow(linha)
    
    def parse(self, response):
        for idSenador in response.css('select.formulario-elemento-destaque option::attr("value")').extract():
            if(Utils.is_empty_string(idSenador) == False):
                self.countIds += 1
                yield scrapy.Request(("http://www6g.senado.gov.br/transparencia/sen/"+idSenador), self.parse_senador)
        print("Numero de senadores "+str(self.countIds))

    def parse_senador(self, response):
        nome = ""
        for post_title in response.css('div.foto > img::attr("alt")').extract():
            nome = Utils.utf8_encode(post_title)

        valor = 0.0
        for post_title in response.css('table.table-striped > tfoot > tr td.valor::text').extract():
            valor = valor + float(Utils.reals_to_float(post_title))

        partido = ""
        for post_title in response.css('div.ProfileSenadores h1 > small::text').extract():
            partido = Utils.utf8_encode(post_title)
        
        row = [nome, partido, valor, Utils.now(), datetime.now().year, response.url]
        self.rows.append(row)