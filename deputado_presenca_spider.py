# coding=utf-8
import scrapy
import string
import csv # ver tambem em https://github.com/chorbl/nfl-reg-season-wins-scrapy-spider/tree/master/tutorial
from datetime import datetime
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
from utils import Utils
import re

class DeputadoPresencaSpider(scrapy.Spider):
    name = 'deputadopresencaspider'
    start_urls = ['http://www2.camara.leg.br/deputados/pesquisa']
    countIds=0
    countNomes=0
    limiteDeputados = 0
    rows = []

     # Metodo para forcar que o metodo spider_closed seja chamado no final da execucao
    def __init__(self):
        dispatcher.connect(self.spider_closed, signals.spider_closed)

    def spider_closed(self, spider):
        with open('deputado_presenca.csv', 'wb') as csvfile:            
            spamwriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            header = ['nome_deputado', 'dias_sessoes_ordinarias', 'dias_presenca_sessoes_ordinarias', 
            'dias_ausencia_justificadas_sessoes_ordinarias','dias_ausencia_nao_justificadas_sessoes_ordinarias', 'data_extracao', 'url']            
            spamwriter.writerow(header)
            for linha in self.rows:
                spamwriter.writerow(linha)
        pass

    def parse(self, response):
        now = datetime.now()
        today = str(now.day)+"/"+str(now.month)+"/"+str(now.year)
        i = 0
        for idSenador in response.css('select#deputado > option::attr("value")').extract():
            if(self.limiteDeputados==0 or i < self.limiteDeputados):
                try:
                    if(idSenador != ""):
                        i = i+1
                        percent = idSenador.index("%")
                        exclamation = idSenador.index("!")
                        print(idSenador[percent+3:exclamation])
                        print("idSEnador:"+idSenador[percent+3:exclamation] + "\n")
                        self.countIds += 1
                        urlCamara = "http://www.camara.gov.br/internet/deputado/RelPresencaPlenario.asp?nuLegislatura=55&nuMatricula="+idSenador[percent+3:exclamation]+"&dtInicio=01/02/2015&dtFim="+today
                        print(urlCamara)
                        print("\n")
                        yield scrapy.Request(urlCamara, self.parse_titles)
                except Exception, e:
                    raise
                else:
                    pass
                finally:
                    pass
        print("Numero de senadores "+str(self.countIds))

    def parse_titles(self, response):
        nome = ""
        for post_title in response.xpath('//*[@id="content"]/h3/a/text()').extract():
            nome = post_title
            nome = Utils.utf8_encode(re.sub('[\t\n\r]', '', nome[0:nome.index(" - ")].strip()))
            print("Nome: ",nome+"\n")

        dias_sessoes_deliberativas = 0
        
        for post_title in response.xpath('//*[@id="content"]/table[2]/tr[1]/td[2]/text()').extract():
            dias_sessoes_deliberativas = post_title.strip()
            print("Dias Sessoes deliberativas: " +dias_sessoes_deliberativas + "\n")        

        dias_presenca_sessoes_deliberativas = 0
        for post_title in response.xpath('//*[@id="content"]/table[2]/tr[2]/td[2]/text()').extract():
            dias_presenca_sessoes_deliberativas = post_title.strip()
            print("Dias dias_presenca_sessoes_deliberativas: "+dias_sessoes_deliberativas + "\n")

        dias_ausencia_justificadas_sessoes_deliberativas= 0
        for post_title in response.xpath('//*[@id="content"]/table[2]/tr[3]/td[2]/text()').extract():
            dias_ausencia_justificadas_sessoes_deliberativas = post_title.strip()
            print("dias_ausencia_justificadas_sessoes_deliberativas: "+ dias_ausencia_justificadas_sessoes_deliberativas+"\n")

        dias_ausencia_nao_justificadas_sessoes_deliberativas=0
        for post_title in response.xpath('//*[@id="content"]/table[2]/tr[4]/td[2]/text()').extract():
            dias_ausencia_nao_justificadas_sessoes_deliberativas = post_title.strip()
            print("dias_ausencia_nao_justificadas_sessoes_deliberativas: " + dias_ausencia_nao_justificadas_sessoes_deliberativas+"\n")

        row = [nome, dias_sessoes_deliberativas, dias_presenca_sessoes_deliberativas, dias_ausencia_justificadas_sessoes_deliberativas, 
        dias_ausencia_nao_justificadas_sessoes_deliberativas, Utils.now(), response.url]

        self.rows.append(row)