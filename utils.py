# coding=utf-8
from datetime import datetime, date
import re

# Classe com funcoes comuns e que podem ser reaproveitadas
class Utils(object):
    @staticmethod
    def utf8_encode(str2):
        return re.sub('\s+',' ',str(str2.encode('utf-8')))

    @staticmethod
    def reals_to_float(str2):
        str2 = str2.replace('.','')
        str2 = str2.replace(",",".")
        return str2

    @staticmethod
    def now():
        now = datetime.now()        
        return str(now.day)+"/"+str(now.month)+"/"+str(now.year)

    @staticmethod
    def convertToDate(str2):
        arr = str2.split("/")
        # print("Recebeu "+str2)
        # retorno = date(int(arr[2]), int(arr[1]), int(arr[0]))
        # retorno = date(2015, 7, 15)
        # return retorno.strftime("%d/%m/%Y")
        return "{0:0>2}".format(arr[0]) + "/" + "{0:0>2}".format(arr[1]) + "/" + arr[2]

    @staticmethod
    def is_empty_string(str2):
        return (True if str2.strip()=="" else False)


