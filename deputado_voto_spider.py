# coding=utf-8
import scrapy
import string
import csv # ver tambem em https://github.com/chorbl/nfl-reg-season-wins-scrapy-spider/tree/master/tutorial
from datetime import datetime
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
from utils import Utils
import re

class BlogSpider(scrapy.Spider):
    name = 'blogspider'
    start_urls = ['http://www2.camara.leg.br/deputados/pesquisa']
    countIds=0
    countNomes=0
    limiteDeputados = 0
    rows = []

    # Metodo para forcar que o metodo spider_closed seja chamado no final da execucao
    def __init__(self):
        dispatcher.connect(self.spider_closed, signals.spider_closed)

    def spider_closed(self, spider):
        with open('deputado_voto.csv', 'wb') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            header = ['nome_deputado', 'sigla', 'voto', 'data_extracao', 'url']
            spamwriter.writerow(header)
            for linha in self.rows:
                spamwriter.writerow(linha)
        pass

    def parse(self, response):
        now = datetime.now()
        today = str(now.day)+"/"+str(now.month)+"/"+str(now.year)
        i = 0
        for idSenador in response.css('select#deputado > option::attr("value")').extract():
            if(self.limiteDeputados==0 or i < self.limiteDeputados):
                i = i+1
                print(idSenador + "\n")
                try:
                    if(idSenador != ""):
                        percent = idSenador.index("%")
                        exclamation = idSenador.index("!")
                        print(idSenador[percent+3:exclamation])
                        self.countIds += 1
                        urlCamara = "http://www.camara.gov.br/internet/deputado/RelVotacoes.asp?nuLegislatura=55&nuMatricula="+idSenador[percent+3:exclamation]+"&dtInicio=01/02/2015&dtFim="+today
                        print(urlCamara)
                        print("\n")
                        yield scrapy.Request(urlCamara, self.parse_titles)
                except Exception, e:
                    raise
                else:
                    pass
                finally:
                    pass
        print("Numero de senadores "+str(self.countIds))

    def parse_titles(self, response):
        nome = ""
        for post_title in response.xpath('//*[@id="content"]/h3/a/text()').extract():
            nome = post_title
            nome = Utils.utf8_encode(re.sub('[\t\n\r]', '', nome[0:nome.index(" - ")].strip()))
            print("Nome: ",nome+"\n")
        pec=[]
        voto = []

        for post_title in response.xpath('//table/tr[not(contains(@class, "even"))]/td[2]/a/text()').extract():
            print('Sessão/Votação2', post_title)
            pec.append(Utils.utf8_encode(post_title))

        for post_title in response.xpath('//table/tr[not(contains(@class, "even"))]/td[4]/text()').extract():
            voto.append(Utils.utf8_encode(post_title))

        for i in range(0,len(pec)-1):
            self.rows.append([nome, pec[i], voto[i], Utils.now(), response.url])