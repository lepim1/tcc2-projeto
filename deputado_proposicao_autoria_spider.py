# coding=utf-8
import scrapy
import string
import csv # ver tambem em https://github.com/chorbl/nfl-reg-season-wins-scrapy-spider/tree/master/tutorial
from datetime import datetime
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
from utils import Utils
import re
import urllib2

class DeputadoProposicaoAutoriaSpider(scrapy.Spider):
    name = 'deputadoproposicaoautoriaspider'
    start_urls = ['http://www2.camara.leg.br/deputados/pesquisa']
    countIds=0    

    limiteDeputados = 10
    rows = []

     # Metodo para forcar que o metodo spider_closed seja chamado no final da execucao
    def __init__(self):
        dispatcher.connect(self.spider_closed, signals.spider_closed)

    def spider_closed(self, spider):
        with open('deputado_proposicao_autoria2.csv', 'wb') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            header = ['nome_deputado', 'proposicao', 'data_apresentacao', 'ementa',
            'situacao', 'data_extracao', 'url']
            spamwriter.writerow(header)
            for linha in self.rows:
                spamwriter.writerow(linha) 
        pass

    #Parse da pagina principal 
    def parse(self, response):
        i = 0
        for idDeputado in response.css('select#deputado > option::attr("value")').extract():
            if(self.limiteDeputados == 0 or i < self.limiteDeputados):
                i = i+1
                try:
                    if(idDeputado != ""):
                        percent = idDeputado.index("|")
                        exclamation = idDeputado.index("%")
                        print(idDeputado[percent+1:exclamation])
                        self.countIds += 1
                        urlCamara = "http://www.camara.gov.br/internet/sileg/Prop_lista.asp?Autor="+idDeputado[percent+1:exclamation]+"&Limite=N&nomeDeputado="+idDeputado[0:idDeputado.index("|")]
                        print(urlCamara)
                        # print("Nome: "+idDeputado[0:idDeputado.index("|")]+"\n")
                        # print("Estado: "+idDeputado[idDeputado.index("!")+1:idDeputado.index("=")]+"\n")
                        # print("Partido: "+idDeputado[idDeputado.index("=")+1:idDeputado.index("?")]+"\n")
                        yield scrapy.Request(urlCamara, self.pagina1)
                except Exception, e:
                    raise
                else:
                    pass
                finally:
                    pass

    #Parse da primeira pagina 1
    def pagina1(self, response):
        self.getProposicaoPageData(response)
        for post_title in response.css('div.listingBar > a::attr(href)').extract():
            yield scrapy.Request("http://www.camara.gov.br"+post_title, self.parsepaginan)

    def getProposicaoPageData(self, response):
        proposicao=[]
        ementa=[]
        situacao=[]
        nome = urllib2.unquote(Utils.utf8_encode(response.url[response.url.index("nomeDeputado=")+len("nomeDeputado="):]))
        urls = []
        data_apresentacao = []
        print("Nome: "+nome+"\n")

        for post_title in response.xpath('//*[@id="frmListaProp"]/table/tbody/tr[2]/td[2]/p[2]/text()[1]').extract():
            data_apresentacao.append(Utils.convertToDate(Utils.utf8_encode(post_title)))

        for post_title in response.css('a.rightIconified::text').extract():
            proposicao.append(Utils.utf8_encode(post_title))

        for post_title in response.xpath('//*[@id="frmListaProp"]/table/tbody/tr/td[3]/text()').extract():
            situacao.append(Utils.utf8_encode(post_title))

        countTbody = len(response.xpath('//*[@id="frmListaProp"]/table/tbody'))
        for i in range(1,countTbody+1):
            try:
                post_title = response.xpath('//*[@id="frmListaProp"]/table/tbody['+str(i)+']/tr[2]/td[2]/p[2]/text()[2]').extract()[0]
            except IndexError:
                post_title = ""
            finally:
                ementa.append(Utils.utf8_encode(post_title))

        for i in range(0, len(proposicao)):
            self.rows.append([nome, proposicao[i], data_apresentacao[i], ementa[i], situacao[i], Utils.now(), response.url])

    #Parse das demais paginas
    def parsepaginan(self, response):
        self.getProposicaoPageData(response)